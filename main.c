// Client side C/C++ program to demonstrate Socket programming
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include "constants.h"

//void sendClientMessage(int socket, char clientMessage[1024], char serverResponse[1024]);

int main(int argc, char const *argv[])
{
    struct sockaddr_in address;
    struct sockaddr_in serv_addr;
    int sock = 0, valread;
    

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("\n Socket creation error \n");
        return -1;
    }

    memset(&serv_addr, '0', sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

          // Convert IPv4 and IPv6 addresses from text to binary form
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) 
    {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0){
        perror(":-(");
    }

    int connectedFirstTime = 0;
    for(;;){

        char clientMessage [1024];
        char serverResponse[1024] = {0};

        if(connectedFirstTime == 0){
            //Show user prompt
            puts("Enter user name: "); 
            connectedFirstTime = 1;  
        }

        //get user input
        gets(clientMessage);
        send(sock , clientMessage , strlen(clientMessage) , 0 );

        //read server response
        valread = read( sock , serverResponse, 1024);
        printf("%s\n",serverResponse );

        //checking if user sent nonsense
        if(strcmp(serverResponse, errorMessageDefault) == 0){
            exit(-1);
        }
        
        if(strcmp(errorMessageDefaultLogin, serverResponse) == 0){
            exit(-1);
        }
    }
return 0;
}

void sendClientMessage(int socket, char clientMessage[100], char serverResponse[1024]){
    int valread;
    //send server a message
    gets(clientMessage);

    send(socket , clientMessage , strlen(clientMessage) , 0 );

    //read server response
    valread = read( socket , serverResponse, 1024);

    if(strcmp(errorMessageDefaultLogin, serverResponse) == 0){
        printf("%s\n",serverResponse );
        exit(-1);
    }
    printf("%s\n",serverResponse );

}
